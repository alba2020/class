package main

import (
	"fmt"
)

// получает строки от пользователя
// () -> []string
func getLines() []string {
	lines := make([]string, 0, 0)
	for {
		var line string
		fmt.Scanf("%s\n", &line)

		if line == "" {
			break
		} else {
			lines = append(lines, line)
		}
	}

	return lines
}

// выводит строки на экран
// []string -> ()
func printLines(lines []string) {
	for i, line := range lines {
		fmt.Printf("[%d] = %s\n", i, line)
	}
}

func main() {
	lines := getLines()

	printLines(lines)
}

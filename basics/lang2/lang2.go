package main

import "fmt"

func main1() {
	// array
	// всегда фиксированный размер
	// var x [5]int
	// var y [2]bool
	// y[0] = true
	// y[1] = false
	// fmt.Println(x, y)

	// slice
	// динамически меняет размер
	// int - тип "целое число"
	// []int - тип "слайс целых чисел"
	// pointer to memory
	// len
	// cap
	var s []int
	// s2 := []int{}
	// t := make([]int, 0)

	fmt.Println("s =", s)
	fmt.Println("len =", len(s))
	fmt.Println("cap =", cap(s))

	s = append(s, 42)
	fmt.Println(s)
	fmt.Println("len =", len(s))
	fmt.Println("cap =", cap(s))

	s = append(s, 1)
	s = append(s, 2)
	s = append(s, 3)
	s = append(s, 4) // меняет len(), может менять cap()

	fmt.Println(s)               // [42 1 2 3 4]
	fmt.Println("len =", len(s)) // 5
	fmt.Println("cap =", cap(s)) // 5

	// functions: len, cap, append, make
	t := make([]int, 10, 128) // make(type, len, cap)
	fmt.Println(len(t), cap(t))

	t[0] = 123
	fmt.Println(t[0])
}

func main() {
	names := []string{"John", "George", "Paul", "Ringo", "Ken", "Brian", "Rob"}
	// make([]string, 7, 7)

	fmt.Println(len(names))
	fmt.Println(cap(names))

	names = append(names, "Pink")

	fmt.Println(names)

	for index, name := range names {
		fmt.Printf("names[%d] = %s\n", index, name)
	}
	for i := range names { // i == 0, 1, 2, ... , 7
		fmt.Println(names[i])
	}
	for _, name := range names {
		fmt.Println(name)
	}

	x := 1000
	fmt.Println(id(x))

	y := 2000
	fmt.Println(x, y)
	x, y = multi(x, y)
	fmt.Println(x, y)

	// client code
	value := 9
	result := square(value) // int -> int
	fmt.Println(result)
}

// functions
// <func> <name> (<params-list>?) <return-type>?

func id(x int) int {
	return x
}

func add(x, y int) int {
	return x + y
}

func multi(a, b int) (int, int) {
	return b, a
}

func eq(a, b int) bool {
	return a == b
}

func foo() {
	fmt.Println("!")
	return
	// not-reachable code
}

// type: function: int -> int
// func(int)int
func square(x int) int {
	return x * x
}

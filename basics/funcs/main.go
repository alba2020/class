package main

import "fmt"

// decorator pattern
func call(f func()) {
	fmt.Println("=before")
	f()
	fmt.Println("=after")
}

func makeDouble(f func()) func() {
	return func() {
		f()
		f()
	}
}

func main() {
	// f1 := func() {
	// 	fmt.Println("hello world!")
	// }

	// call(f1)

	double := makeDouble(func() {
		fmt.Println("hello world!")
	})
	double()
}

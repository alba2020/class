package main

import "fmt"

// PascalCaseVariable
// camelCaseVariable <- Golang Way!
// the_snake_case
// the-kebab-case
// PHP_CONSTANT_VAL

func printSlice(numbers []int) {
	for i, x := range numbers {
		fmt.Printf("[%d] = %d ", i, x)
	}
	fmt.Printf("\n")
}

// []int -> int
func maxElement(numbers []int) int {
	maxElement := numbers[0]

	for _, currentElement := range numbers {
		if currentElement > maxElement {
			maxElement = currentElement
		}
	}

	return maxElement
}

func main() {
	longSliceOfNumbers := []int{1, 2, 3, 4, 2, 3, 0, 10, 2}

	printSlice(longSliceOfNumbers)

	if len(longSliceOfNumbers) == 0 {
		fmt.Println("empty slice!")
		return
	}

	max := maxElement(longSliceOfNumbers)
	fmt.Printf("max = %d\n", max)
}

package main

import "fmt"

// type slice struct {
// 	array unsafe.Pointer
// 	len   int
// 	cap   int
// }

func printSlice(s string, a []int) {
	fmt.Printf("%p - %v\tlen:%d\tcap:%d\t%s\n", a, a, len(a), cap(a), s)
}

func surprise(a []int) {
	printSlice("sur 1", a)
	a = append(a, 5)
	printSlice("sur 2", a)

	for i := range a {
		a[i] = 5
	}
}

func main() {
	a := []int{1, 2, 3, 4}

	printSlice("main 1", a)
	a = append(a, 5)
	printSlice("main 2", a)

	surprise(a)

	fmt.Println(a)
}

// [ 1 2 3 4 5]

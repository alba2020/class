package main

import (
	"fmt"
	"math/rand"
)

// мораль истории: зависимость - зло

func getName() string {
	return "George"
}

func printSlice(numbers []int) {
	for _, num := range numbers {
		fmt.Print(num, " ")
	}
	fmt.Println()
}

func main() {
	// greeting := formatGreeting(getName(), 11)
	// fmt.Print(greeting)

	// sayHello(4)
	// fmt.Print(badFormatGreeting(10))

	// работа со слайсом
	numbers := make([]int, 10, 10)
	for i := range numbers {
		numbers[i] = rand.Intn(10) // генерит случайные числа от 0 до 9
	}

	printSlice(numbers)

	// count5, count2 := getCount(numbers)
	fmt.Println("count 5 = ", getCount(numbers, 5))
	fmt.Println("count 2 = ", getCount(numbers, 2))
}

func getCount(numbers []int, value int) int {
	var count int
	for idx := range numbers {
		if numbers[idx] == value {
			count++
		}
	}
	return count
}

// число элементов, равных чему-то
// func getCount(numbers []int) (int, int) {
// 	var count5 int
// 	for idx := range numbers {
// 		if numbers[idx] == 5 {
// 			count5++
// 		}
// 	}
// 	var count2 int
// 	for idx := range numbers {
// 		if numbers[idx] == 2 {
// 			count2++
// 		}
// 	}
// 	return count5, count2
// }

// int -> string
// но внутри она работает с глобальной name
// func badFormatGreeting(hours int) string {
// 	template := "Hello, %s! Please be at my place at %d pm!\n"
// 	result := fmt.Sprintf(template, name, hours)
// 	return result
// }

// math function
// (string, int) -> string
// black box
func formatGreeting(name string, hours int) string {
	template := "Hello, %s! Please be at my place at %d pm!\n"
	result := fmt.Sprintf(template, name, hours)
	return result
}

// int -> ()
// NOT a black box
func sayHello(n int) {
	for i := 0; i < n; i++ {
		fmt.Println("Hello!")
	}
	return
}

package main

import "fmt"

func swap(px *int, py *int) {
	// px *int - px - указатель на int
	// *px - "взять значение по адресу px"
	// var x int = 0
	// px = &x - в px находится адрес переменной x

	tmp := *px // значение по адресу px
	*px = *py
	*py = tmp
}

func main1() {
	a := 0
	b := 1

	fmt.Printf("a = %d b = %d\n", a, b)

	// call it!
	swap(&a, &b)

	fmt.Printf("a = %d b = %d\n", a, b)
}

// a = 0 b = 1
// a = 1 b = 0

func main() {
	var x int = 10
	var px *int = &x

	fmt.Println(x)
	*px = 42
	fmt.Println(x)

	fmt.Println(*px, px)
}

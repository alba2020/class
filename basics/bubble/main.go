package main

import "fmt"

// time = O(n)
// xs[n] = 10 2 5 15 0
// содержит ли xs x ? // 4

// time = O(log2n)
// xs[n] = 0 2 5 10 15

func bubbleSort(a []int) {
	last := len(a) - 1
	for i := 0; i < last; i++ {
		for j := i + 1; j <= last; j++ {
			if a[i] > a[j] {
				tmp := a[i]
				a[i] = a[j]
				a[j] = tmp
			}
		}
	}
}

func main() {
	a := []int{1, 5, 10, 2, 0, 4, 3}

	fmt.Println(a)

	// идея: сравнить попарно все элементы и сделать так, чтобы правый был больше левого
	bubbleSort(a)

	fmt.Println(a)
}

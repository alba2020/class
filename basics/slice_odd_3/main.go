package main

import "fmt"

// type slice struct {
// 	array unsafe.Pointer
// 	len   int
// 	cap   int
// }

func printSlice(s string, a []int) {
	fmt.Printf("%p - %v\tlen:%d\tcap:%d\t%s\n", a, a, len(a), cap(a), s)
}

func surprise(a []int) {
	a = append(a, 5)

	printSlice("inside surprise", a)

	for i := range a {
		a[i] = 5
	}
}

func main() {
	a := []int{1, 2, 3, 4}
	printSlice("before", a)

	surprise(a)

	a = append(a, 5)
	printSlice("after append", a)
	fmt.Println(a)
}
